def traverse(n, pathes):
    def dijkstra(node, price, used):
        old_used = used.copy()
        for adj in pathes[node]:
            if adj not in used:
                price = min(price, dijkstra(adj, price + 1, used))
                used = old_used
        return price

    return dijkstra(1, 0, {})


def input_handler():
    n, m = map(int, input().split())
    pathes = {}
    for _ in range(m):
        path = tuple(map(int, input().split()))
        if path[0] not in pathes:
            pathes[path[0]] = [path[1]]
        else:
            pathes[path[0]].append(path[1])

        if path[1] not in pathes:
            pathes[path[1]] = [path[0]]
        else:
            pathes[path[1]].append(path[0])

    print(traverse(n, pathes))


input_handler()