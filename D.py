def unbox(boxes):
    for box in boxes.values():
        if box == 1:
            return 'No'
    return 'Yes'


def input_handler():
    n, m = map(int, input().split())
    boxes = {i: 0 for i in range(1, n + 1)}
    for i in range(m):
        input()
        balls = map(int, input().split())
        for idx, ball in enumerate(balls):
            boxes[ball] ^= 1
    print('n - ', n, '\nm - ', m)

    print('after boxes - ', boxes)

    #print(boxes)
    print(unbox(boxes))

input_handler()