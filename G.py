def replacements(pairs):
    return ' '. join(str(num) for pair in sorted(pairs) for num in pair)


def input_handler():
    n, m = map(int, input().split())
    pairs = []
    for i in range(m):
        pairs.append(map(int, input().split()))
    replacements(pairs)


def test():
    n, m = 4, 3
    pairs = [(2, 1), (3, 4)]
    print('2 1 3 4' == replacements(pairs))

    # second case not visible on video

test()