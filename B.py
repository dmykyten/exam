import math

def squares(points):
    total = 0
    # look for paralel to oy
    for i, first_p in enumerate(points):
        for j, second_p in enumerate(points):
            if second_p == first_p:
                continue
            intersect = points[first_p].intersection(points[second_p])
            size = len(intersect)
            if size >= 2:
                pairs = math.factorial(size) // (math.factorial(size - 2) * math.factorial(2))
                total += pairs
                #print(first_p, second_p, '||', intersect)
                #print(size, pairs)
            else:
                #print(first_p, second_p, '||', intersect)
                #print(size, 'too small')
                pass

    print(total // 2)


def input_handler():
    n = int(input())
    points = {}
    for _ in range(n):
        point = tuple(map(int, input().split()))
        if point[0] not in points:
            points[point[0]] = set()
            points[point[0]].add(point[1])
        else:
            points[point[0]].add(point[1])
        """
        if point[1] not in points['y']:
            points['y'][point[1]] = set()
            points['y'][point[1]].add(point[0])
        else:
            points['y'][point[1]].add(point[0])
        """
    squares(points)

input_handler()
"""
6
0 0
0 1
1 0
1 1
2 0
2 1

"""