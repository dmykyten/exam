def counter(x, y, menus):
    menus.sort(key=(lambda x: x[0] + x[1]), reverse=True)
    counter = 0
    for menu in menus:
        x -= menu[0]
        y -= menu[1]
        if x <= 0 and y <= 0:
            return counter + 1
        counter += 1

    if x > 0 or y > 0:
        return -1
    else:
        return counter


def input_handler():
    n = int(input())
    x, y = map(int, input().split())
    menus = []
    for _ in range(n):
        menus.append(tuple(map(int, input().split())))
    print(counter(x, y, menus))


def test():
    n = 3
    x, y = 5, 6
    menus = [(2, 1), (3, 4), (2, 3)]
    print(2 == counter(x, y, menus))

    n = 3
    x, y = 8, 8
    menus = [(3, 4), (2, 3), (2, 1)]
    print(-1 == counter(x, y, menus))

#input_handler()
test()