import math


def min_diff(seq1, seq2):
    seq1.extend(seq2)
    print(seq1)
    seq1.sort(key=lambda x: x[0])
    diff = math.inf
    for i in range(len(seq1) - 1):
        if seq1[i][1] != seq1[i + 1][1]:
            diff = min(diff, abs(seq1[i][0] - seq1[i + 1][0]))

    print(diff)
"""
def min_diff(seq1, seq2):
    seq1.sort()
    seq2.sort()
    diff = math.inf
    for elem in seq1:
        for elem2 in seq2:
            tmp = abs(elem2 - elem)
            if tmp < diff:
                diff = tmp
    print(diff)
"""

def input_handler():
    n, m = map(int, input().split())
    seq1 = list(map(lambda x: (int(x), 0), input().split()))
    seq2 = list(map(lambda x: (int(x), 1), input().split()))
    min_diff(seq1, seq2)

def test():
    seq1 = [1, 6]
    seq2 = [4 ,9]
    seq1 = list(map(lambda x: (int(x), 0), seq1))
    seq2 = list(map(lambda x: (int(x), 1), seq2))
    min_diff(seq1, seq2)

    seq1 = [10]
    seq2 = [10]
    seq1 = list(map(lambda x: (int(x), 0), seq1))
    seq2 = list(map(lambda x: (int(x), 1), seq2))
    min_diff(seq1, seq2)

    seq1 = [82, 76, 82, 82, 71, 70]
    seq2 = [17, 39, 67, 2, 45, 35, 22, 24]
    seq1 = list(map(lambda x: (int(x), 0), seq1))
    seq2 = list(map(lambda x: (int(x), 1), seq2))
    min_diff(seq1, seq2)

test()