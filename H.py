import itertools


def clique(seq, k):
    neighbors = {}
    for pair in seq:
        if pair[0] in neighbors:
            neighbors[pair[0]].add(pair[1])
        else:
            neighbors[pair[0]] = set()
            neighbors[pair[0]].add(pair[1])

        if pair[1] in neighbors:
            neighbors[pair[1]].add(pair[0])
        else:
            neighbors[pair[1]] = set()
            neighbors[pair[1]].add(pair[0])

    print(neighbors)

    for comb in itertools.combinations(neighbors, k):
        valid = True
        print('comb -', comb)
        combined = set(comb)
        for node in comb:
            if list(combined.difference(neighbors[node]))[0] != node:
                valid = False
                break

        print('combined -', combined)
        if not valid:
            continue
        return 'YES'

    return 'NO'


def input_handler():
    k = 4
    nodes, edges = map(int, input().split())
    seq = [(a, b) for a, b in input().split()]
    input()
    clique(seq, k)


def test():
    k = 4
    nodes, edges = 4, 6
    seq = [(1, 2), (2, 3), (3, 4), (1, 4), (1, 3), (2, 4)]
    print(clique(seq, k))

#input_handler()
test()
