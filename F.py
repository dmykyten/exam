def travel(xs, ys, xt, yt, planets):
    if xs != xt:
        # y = kx + b
        k = (ys - yt) / (xs - xt)
        b = ys - xs * k
        #print(k, b)

    else:
        # on same x
        pass

def input_handler():
    xs, ys, xt, yt = map(int, input().split())
    print('start', (xs, ys), 'end', (xt, yt))
    n = int(input())
    planets = []
    for _ in range(n):
        planet = tuple(map(int, input().split()))
        planets.append(planet)
    print('planets(x, y, r)', planets)
    travel(xs, ys, xt, yt, planets)

def test():
    xs, ys, xt, yt = -2, -2, 2, 2
    n = 1
    planets = [(0, 0, 1)]
    travel(xs, ys, xt, yt, planets)

#input_handler()
test()